import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display main components in main page', () => {
    page.navigateTo();
    expect(page.getComponentByName('app-toolbar')).toBeTruthy();
    expect(page.getComponentByName('app-personal-information')).toBeTruthy();
    expect(page.getComponentByName('app-experience')).toBeTruthy();
    expect(page.getComponentByName('app-extracurricular')).toBeTruthy();
    expect(page.getComponentByName('app-certifications')).toBeTruthy();
    expect(page.getComponentByName('app-education')).toBeTruthy();
  });
});
