import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getComponentByName(name: string): Promise<boolean> {
    return element(by.css(name)).isPresent() as Promise<boolean>;
  }
}
