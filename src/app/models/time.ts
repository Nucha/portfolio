export interface Time {
  years: number;
  months: number;
}
