export interface Profile {
  name: string;
  role: string;
  description: string;
  socialLinks: SocialLink[];
  contactDetails: ContactDetail[];
  experience: Experience[];
  extracurricular: Extracurricular[];
  certifications: Certificate[];
  education: EducationRecord[];
}

export interface SocialLink {
  name: string;
  url: string;
  icon: string;
}

export interface ContactDetail {
  key: string;
  value: string;
  icon: string;
}

export interface Experience {
  company: string;
  location: string;
  industry: string;
  positions: Position[];
}

export interface Extracurricular {
  organization: string;
  role: string;
  location: string;
  date: string;
  detail: string;
  resources?: ExtracurricularResource[];
}

export interface ExtracurricularResource {
  name: string;
  url: string;
}

export interface Position {
  role: string;
  client?: Client;
  startDate: string;
  endDate?: string;
  mainTasks: string[];
}

export interface Client {
  company: string;
  industry: string;
}

export interface Certificate {
  name: string;
  company: string;
  year: number;
  link: string;
}

export interface EducationRecord {
  institute: string;
  grade: string;
  title: string;
  startYear: number;
  endYear: number;
  status: string;
}
