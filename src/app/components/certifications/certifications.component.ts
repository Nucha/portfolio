import { Component } from '@angular/core';

import profile from '@assets/profile.en.json';
import { Certificate } from 'src/app/models/profile';

@Component({
  selector: 'app-certifications',
  templateUrl: './certifications.component.html',
  styleUrls: ['./certifications.component.scss'],
})
export class CertificationsComponent {
  title = 'Certifications';
  certifications: Certificate[] = profile.certifications;

  constructor() {}
}
