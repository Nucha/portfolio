import { Component, Input } from '@angular/core';

import { Certificate } from 'src/app/models/profile';
import { allowedEvents, GoogleAnalyticsService } from '@services/google-analytics/google-analytics.service';

@Component({
  selector: 'app-certificate',
  templateUrl: './certificate.component.html',
  styleUrls: ['./certificate.component.scss'],
})
export class CertificateComponent {
  @Input() certificate: Certificate;

  constructor(private analytics: GoogleAnalyticsService) {}

  onClick(certificate: Certificate): void {
    const params: { [key: string]: any } = {
      certificate: certificate.name,
    };
    this.analytics.logEvent(allowedEvents.CERTIFICATE, params);
  }
}
