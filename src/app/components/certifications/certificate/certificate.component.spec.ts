import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { CertificateComponent } from './certificate.component';
import { allowedEvents, GoogleAnalyticsService } from '@services/google-analytics/google-analytics.service';
import { GoogleAnalyticsServiceMock } from '@services/google-analytics/google-analytics.service.mock';

describe('CertificateComponent', () => {
  let component: CertificateComponent;
  let fixture: ComponentFixture<CertificateComponent>;
  let analytics: GoogleAnalyticsServiceMock;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [CertificateComponent],
      providers: [
        {
          provide: GoogleAnalyticsService,
          useClass: GoogleAnalyticsServiceMock,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertificateComponent);
    component = fixture.componentInstance;
    component.certificate = {
      name: 'Innovation Seminar',
      company: 'U.T.N',
      year: 2018,
      link: 'assets/certificates/INNOVATION.jpeg',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should log certificate analytics event when some certificate is press', () => {
    analytics = TestBed.inject(GoogleAnalyticsService);
    const logEventSpy = spyOn(analytics, 'logEvent');
    const expectedObject = { certificate: component.certificate.name };
    component.onClick(component.certificate);
    expect(analytics).toBeTruthy();
    expect(logEventSpy).toHaveBeenCalledWith(allowedEvents.CERTIFICATE, expectedObject);
  });
});
