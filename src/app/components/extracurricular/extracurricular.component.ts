import { Component } from '@angular/core';

import { Extracurricular } from '@models/profile';
import profile from '@assets/profile.en.json';

@Component({
  selector: 'app-extracurricular',
  templateUrl: './extracurricular.component.html',
  styleUrls: ['./extracurricular.component.scss'],
})
export class ExtracurricularComponent {
  title = 'Extracurricular';
  experiences: Extracurricular[] = profile.extracurricular;

  constructor() {}
}
