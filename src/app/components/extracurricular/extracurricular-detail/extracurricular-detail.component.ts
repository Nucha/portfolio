import { Component, Input } from '@angular/core';

import { Extracurricular, ExtracurricularResource } from '@models/profile';
import { allowedEvents, GoogleAnalyticsService } from '@services/google-analytics/google-analytics.service';

@Component({
  selector: 'app-extracurricular-detail',
  templateUrl: './extracurricular-detail.component.html',
  styleUrls: ['./extracurricular-detail.component.scss'],
})
export class ExtracurricularDetailComponent {
  @Input() experience: Extracurricular;

  constructor(private analytics: GoogleAnalyticsService) {}

  onClick(experience: Extracurricular): void {
    this.analytics.logEvent(allowedEvents.EXTRACURRICULAR, {
      experience: experience.role,
    });
  }

  onClickResource(resource: ExtracurricularResource): void {
    this.analytics.logEvent(allowedEvents.EXTRACURRICULAR_RESOURCE, {
      resource: resource.name,
    });
  }
}
