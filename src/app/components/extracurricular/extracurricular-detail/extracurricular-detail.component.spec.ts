import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { ExtracurricularDetailComponent } from './extracurricular-detail.component';
import { GoogleAnalyticsServiceMock } from '@services/google-analytics/google-analytics.service.mock';
import { allowedEvents, GoogleAnalyticsService } from '@services/google-analytics/google-analytics.service';

describe('ExtracurricularDetailComponent', () => {
  let component: ExtracurricularDetailComponent;
  let fixture: ComponentFixture<ExtracurricularDetailComponent>;
  let analytics: GoogleAnalyticsServiceMock;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [ExtracurricularDetailComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExtracurricularDetailComponent);
    component = fixture.componentInstance;
    component.experience = {
      organization: 'Buenos Aires Government',
      role: 'Entrepreneur Competition',
      location: 'Argentina',
      date: 'August 2017',
      detail:
        'I have taken part in an entrepreneurship competition for more than 2000 participants called "Potenciate" launched by the government, where I ended up as one of three finalist for the first prize. I represented the business model using "CANVAS" and made the "SWOT" analysis, also I have taken a training in Elevator Pitch. The final was broadcasted on digital TV. The project called "Juga Tu Partido" is aimed to sport players who want to find other close players to play a match of any sport. They can contact each other using the app chat for organizing the match. Also, they are able to use the interactive map to find the right place to play the match and book it. Finally, the users are able to schedule an event for the matches and closer users will receive the notification to join.',
      resources: [
        {
          name: 'First Instance',
          url: 'https://potenciate.buenosaires.gob.ar/emprendedurismo/nota/88/potenciate_nuevos_medios_90_semifinalistas',
        },
        {
          name: 'Second Instance',
          url: 'https://potenciate.buenosaires.gob.ar/emprendedurismo/nota/113/potenciate_nuevos_medios_finalistas',
        },
        {
          name: 'Final Instance - live transmission',
          url: 'https://api.fwtv.tv/embed/potenciate/potenciate-nuevos-medios-la-final?utm_medium=referral',
        },
      ],
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should log extracurricular analytics event when some experience is expanded', () => {
    analytics = TestBed.inject(GoogleAnalyticsService);
    const logEventSpy = spyOn(analytics, 'logEvent');
    const expectedObject = { experience: component.experience.role };
    component.onClick(component.experience);
    expect(analytics).toBeTruthy();
    expect(logEventSpy).toHaveBeenCalledWith(allowedEvents.EXTRACURRICULAR, expectedObject);
  });

  it('should log extracurricular analytics event when some experience resource is clicked', () => {
    analytics = TestBed.inject(GoogleAnalyticsService);
    const logEventSpy = spyOn(analytics, 'logEvent');
    const expectedObject = { resource: component.experience.resources[0].name };
    component.onClickResource(component.experience.resources[0]);
    expect(analytics).toBeTruthy();
    expect(logEventSpy).toHaveBeenCalledWith(allowedEvents.EXTRACURRICULAR_RESOURCE, expectedObject);
  });
});
