import { Component } from '@angular/core';

import profile from '@assets/profile.en.json';
import { Experience } from '@models/profile';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss'],
})
export class ExperienceComponent {
  title = 'Experience';
  jobs: Experience[] = profile.experience;

  constructor() {}
}
