import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { JobRoleDetailComponent } from './job-role-detail.component';

describe('RoleDetailComponent', () => {
  let component: JobRoleDetailComponent;
  let fixture: ComponentFixture<JobRoleDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [JobRoleDetailComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobRoleDetailComponent);
    component = fixture.componentInstance;
    component.role = {
      role: 'Test Automation Developer',
      startDate: '12/01/2015',
      endDate: '12/01/2016',
      mainTasks: [
        'Plan and document unit and integration tests.',
        'Develop unit and integration tests using Junit and Selenium WebDriver.',
        'Prepare data on different Oracle databases to satisfy planned tests.',
        'Detect and report bugs.',
      ],
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
