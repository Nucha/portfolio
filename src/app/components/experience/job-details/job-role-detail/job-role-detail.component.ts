import { Component, Input, OnInit } from '@angular/core';

import { Position } from '@models/profile';

@Component({
  selector: 'app-role-detail',
  templateUrl: './job-role-detail.component.html',
  styleUrls: ['./job-role-detail.component.scss'],
})
export class JobRoleDetailComponent implements OnInit {
  @Input() role: Position;
  constructor() {}

  ngOnInit(): void {}
}
