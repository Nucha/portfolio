import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { JobDetailsComponent } from './job-details.component';
import { allowedEvents, GoogleAnalyticsService } from '@services/google-analytics/google-analytics.service';
import { GoogleAnalyticsServiceMock } from '@services/google-analytics/google-analytics.service.mock';

describe('JobDetailsComponent', () => {
  let component: JobDetailsComponent;
  let fixture: ComponentFixture<JobDetailsComponent>;
  let analytics: GoogleAnalyticsServiceMock;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [JobDetailsComponent],
      providers: [
        {
          provide: GoogleAnalyticsService,
          useClass: GoogleAnalyticsServiceMock,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobDetailsComponent);
    component = fixture.componentInstance;
    component.job = {
      company: 'Indra SI S.A.',
      location: 'Argentina',
      industry: 'Consulting Services',
      positions: [
        {
          role: 'Test Analyst',
          client: {
            company: 'Telefónica S.A',
            industry: 'Telecommunications Services',
          },
          startDate: '11/01/2014',
          endDate: '12/01/2015',
          mainTasks: [
            'Plan, execute and document different tests cases such as unit, integration and regression tests.',
            'Prepare data on different Oracle databases to satisfy planned tests.',
            'Create and maintain database´s scripts using Oracle PL/SQL.',
            'Consume JMS web services using SoapUI for detecting and reporting bugs.',
          ],
        },
      ],
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should log job analytics event when some experience is expanded', () => {
    analytics = TestBed.inject(GoogleAnalyticsService);
    const logEventSpy = spyOn(analytics, 'logEvent');
    const expectedObject = { company: component.job.company };
    component.onClick(component.job);
    expect(analytics).toBeTruthy();
    expect(logEventSpy).toHaveBeenCalledWith(allowedEvents.JOB_EXPERIENCE, expectedObject);
  });
});
