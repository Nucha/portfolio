import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PeriodDetailComponent } from './period-detail.component';

describe('PeriodDetailComponent', () => {
  let component: PeriodDetailComponent;
  let fixture: ComponentFixture<PeriodDetailComponent>;
  const expectedRole = {
    role: 'Test Automation Developer',
    startDate: '12/01/2015',
    endDate: '12/01/2016',
    mainTasks: [
      'Plan and document unit and integration tests.',
      'Develop unit and integration tests using Junit and Selenium WebDriver.',
      'Prepare data on different Oracle databases to satisfy planned tests.',
      'Detect and report bugs.',
    ],
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [PeriodDetailComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodDetailComponent);
    component = fixture.componentInstance;
    component.role = expectedRole;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call calculateSinglePosition method', () => {
    const spy = spyOn<any>(component, 'calculateSinglePosition');
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  it('should call calculateAllPositions method', () => {
    component.role = null;
    const spy = spyOn<any>(component, 'calculateAllPositions');
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  it('should calculate Single Position with end date', () => {
    const spyTimeDifference = spyOn<any>(component, 'timeDifference');
    const spyBuildTimeDifferenceLabel = spyOn<any>(component, 'buildTimeDifferenceLabel');
    component['calculateSinglePosition']();
    expect(component.startDate).toEqual(expectedRole.startDate);
    expect(component.endDate).toEqual(expectedRole.endDate);
    expect(spyTimeDifference).toHaveBeenCalled();
    expect(spyBuildTimeDifferenceLabel).toHaveBeenCalled();
  });

  it('should calculate Single Position without end date', () => {
    component.role.endDate = null;
    const spyTimeDifference = spyOn<any>(component, 'timeDifference');
    const spyBuildTimeDifferenceLabel = spyOn<any>(component, 'buildTimeDifferenceLabel');
    component['calculateSinglePosition']();
    expect(component.startDate).toEqual(expectedRole.startDate);
    expect(component.endDate).toEqual(null);
    expect(spyTimeDifference).toHaveBeenCalled();
    expect(spyBuildTimeDifferenceLabel).toHaveBeenCalled();
  });

  it('should calculate All Position with only one role', () => {
    component.roles = [expectedRole];
    const spy = spyOn<any>(component, 'calculateSinglePosition');
    component['calculateAllPositions']();
    expect(spy).toHaveBeenCalled();
  });

  it('should calculate All Position for more than one role', () => {
    component.roles = [expectedRole, expectedRole];
    const spyTimeDifference = spyOn<any>(component, 'timeDifference');
    const spyBuildTimeDifferenceLabel = spyOn<any>(component, 'buildTimeDifferenceLabel');
    component['calculateAllPositions']();
    expect(spyTimeDifference).toHaveBeenCalled();
    expect(spyBuildTimeDifferenceLabel).toHaveBeenCalled();
  });
});
