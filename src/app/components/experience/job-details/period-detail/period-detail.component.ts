import { Component, Input, OnInit } from '@angular/core';

import { Position } from '@models/profile';
import { DateUtilsService } from '@services/date-utils/date-utils.service';
import { Time } from '@models/time';

@Component({
  selector: 'app-period-detail',
  templateUrl: './period-detail.component.html',
  styleUrls: ['./period-detail.component.scss'],
})
export class PeriodDetailComponent implements OnInit {
  @Input() role: Position;
  @Input() roles: Position[];

  presentLabel = 'Present';
  andLabel = 'and';
  startDate: string;
  endDate: string;
  periodLabel: string;

  constructor(private dateUtilsService: DateUtilsService) {}

  ngOnInit(): void {
    this.role ? this.calculateSinglePosition() : this.calculateAllPositions();
  }

  private calculateSinglePosition(): void {
    this.startDate = this.role.startDate;
    this.role.endDate ? (this.endDate = this.role.endDate) : (this.endDate = null);
    const difference: Time = this.timeDifference();
    this.buildTimeDifferenceLabel(difference);
  }

  private calculateAllPositions(): void {
    if (this.roles.length === 1) {
      this.role = this.roles[0];
      this.calculateSinglePosition();
    } else {
      const startDates: Date[] = this.roles.map((role: Position) => new Date(role.startDate));
      this.startDate = Math.min.apply(null, startDates);
      const endDates: Date[] = this.roles.map((role: Position) => new Date(role.endDate));
      if (startDates.length === endDates.length) {
        this.endDate = Math.max.apply(null, endDates);
      }
      const difference: Time = this.timeDifference();
      this.buildTimeDifferenceLabel(difference);
    }
  }

  private timeDifference(): Time {
    const initialDate = new Date(this.startDate);
    let finalDate;
    this.endDate ? (finalDate = new Date(this.endDate)) : (finalDate = new Date());
    return this.dateUtilsService.getTimeDifferenceInYearsAndMonths(initialDate, finalDate);
  }

  private buildTimeDifferenceLabel(difference: Time): void {
    let labelYear;
    let labelMonth;
    difference.years > 1 ? (labelYear = 'years') : (labelYear = 'year');
    difference.months > 1 ? (labelMonth = 'months') : (labelMonth = 'month');
    if (difference.years === 0 && difference.months >= 1) {
      this.periodLabel = `${difference.months} ${labelMonth}`;
    }
    if (difference.years >= 1 && difference.months === 0) {
      this.periodLabel = `${difference.years} ${labelYear}`;
    }
    if (difference.years >= 1 && difference.months >= 1) {
      this.periodLabel = `${difference.years} ${labelYear} ${this.andLabel} ${difference.months} ${labelMonth}`;
    }
  }
}
