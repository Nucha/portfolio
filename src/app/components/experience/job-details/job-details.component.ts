import { Component, Input } from '@angular/core';

import { Experience } from 'src/app/models/profile';
import { allowedEvents, GoogleAnalyticsService } from '@services/google-analytics/google-analytics.service';

@Component({
  selector: 'app-job-details',
  templateUrl: './job-details.component.html',
  styleUrls: ['./job-details.component.scss'],
})
export class JobDetailsComponent {
  @Input() job: Experience;

  constructor(private analytics: GoogleAnalyticsService) {}

  onClick(job: Experience): void {
    this.analytics.logEvent(allowedEvents.JOB_EXPERIENCE, {
      company: job.company,
    });
  }
}
