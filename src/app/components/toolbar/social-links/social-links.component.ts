import { Component } from '@angular/core';

import profile from '@assets/profile.en.json';
import { SocialLink } from 'src/app/models/profile';
import { GoogleAnalyticsService, allowedEvents } from '@services/google-analytics/google-analytics.service';

@Component({
  selector: 'app-account-links',
  templateUrl: './social-links.component.html',
  styleUrls: ['./social-links.component.scss'],
})
export class SocialLinksComponent {
  socialLinks: SocialLink[] = profile.socialLinks;

  constructor(private analytics: GoogleAnalyticsService) {}

  onClick(link: SocialLink): void {
    this.analytics.logEvent(allowedEvents.SOCIAL_MEDIA, { name: link.name });
  }
}
