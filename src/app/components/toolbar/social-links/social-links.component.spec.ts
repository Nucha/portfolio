import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';

import { SocialLinksComponent } from './social-links.component';
import { allowedEvents, GoogleAnalyticsService } from '@services/google-analytics/google-analytics.service';
import { GoogleAnalyticsServiceMock } from '@services/google-analytics/google-analytics.service.mock';

describe('AccountLinksComponent', () => {
  let component: SocialLinksComponent;
  let fixture: ComponentFixture<SocialLinksComponent>;
  let analytics: GoogleAnalyticsServiceMock;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [SocialLinksComponent],
      imports: [MatTooltipModule],
      providers: [
        {
          provide: GoogleAnalyticsService,
          useClass: GoogleAnalyticsServiceMock,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.socialLinks = [
      {
        name: 'LinkedIn',
        url: 'https://linkedin.com/in/nicolas-marcelo-ucha',
        icon: 'assets/images/linkedIn.svg',
      },
    ];
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should log job analytics event when some experience is expanded', () => {
    analytics = TestBed.inject(GoogleAnalyticsService);
    const logEventSpy = spyOn(analytics, 'logEvent');
    const expectedObject = { name: component.socialLinks[0].name };
    component.onClick(component.socialLinks[0]);
    expect(analytics).toBeTruthy();
    expect(logEventSpy).toHaveBeenCalledWith(allowedEvents.SOCIAL_MEDIA, expectedObject);
  });
});
