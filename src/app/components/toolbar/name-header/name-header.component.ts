import { Component } from '@angular/core';

import profile from '@assets/profile.en.json';
import { Profile } from 'src/app/models/profile';

@Component({
  selector: 'app-name-header',
  templateUrl: './name-header.component.html',
  styleUrls: ['./name-header.component.scss'],
})
export class NameHeaderComponent {
  profile: Profile = profile;
  constructor() {}
}
