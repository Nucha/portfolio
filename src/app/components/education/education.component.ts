import { Component } from '@angular/core';

import profile from '@assets/profile.en.json';
import { EducationRecord } from 'src/app/models/profile';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss'],
})
export class EducationComponent {
  title = 'Education';
  education: EducationRecord[] = profile.education;

  constructor() {}
}
