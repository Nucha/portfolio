import { Component, Input } from '@angular/core';

import { EducationRecord } from 'src/app/models/profile';

@Component({
  selector: 'app-institute-details',
  templateUrl: './institute-details.component.html',
  styleUrls: ['./institute-details.component.scss'],
})
export class InstituteDetailsComponent {
  @Input() institute: EducationRecord;

  constructor() {}
}
