import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { InstituteDetailsComponent } from './institute-details.component';

describe('InstituteDetailsComponent', () => {
  let component: InstituteDetailsComponent;
  let fixture: ComponentFixture<InstituteDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [InstituteDetailsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstituteDetailsComponent);
    component = fixture.componentInstance;
    component.institute = {
      institute: 'U.T.N',
      grade: 'Degree ',
      title: 'Electronic Engineering',
      startYear: 2013,
      endYear: 2015,
      status: 'Incomplete',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
