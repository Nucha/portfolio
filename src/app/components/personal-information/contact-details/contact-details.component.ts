import { Component } from '@angular/core';

import profile from '@assets/profile.en.json';
import { ContactDetail } from 'src/app/models/profile';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.scss'],
})
export class ContactDetailsComponent {
  contactDetails: ContactDetail[] = profile.contactDetails;

  constructor() {}
}
