import { Component, Input } from '@angular/core';

import { ContactDetail } from 'src/app/models/profile';

@Component({
  selector: 'app-detail-item',
  templateUrl: './detail-item.component.html',
  styleUrls: ['./detail-item.component.scss'],
})
export class DetailItemComponent {
  @Input() detail: ContactDetail;

  constructor() {}
}
