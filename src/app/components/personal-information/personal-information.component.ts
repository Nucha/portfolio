import { Component } from '@angular/core';

import profile from '@assets/profile.en.json';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss'],
})
export class PersonalInformationComponent {
  description: string = profile.description;

  constructor() {}
}
