import { Injectable } from '@angular/core';
import { GoogleAnalyticsService } from '@services/google-analytics/google-analytics.service';

@Injectable()
export class GoogleAnalyticsServiceMock extends GoogleAnalyticsService {
  logUser(): void {}

  logEvent(eventName: string, params?: { [key: string]: any }): void {}
}
