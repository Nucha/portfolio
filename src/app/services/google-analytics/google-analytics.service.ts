import { Injectable } from '@angular/core';

import { environment } from '@environments/environment';

export enum allowedEvents {
  SOCIAL_MEDIA = 'SOCIAL_MEDIA',
  JOB_EXPERIENCE = 'JOB_EXPERIENCE',
  EXTRACURRICULAR = 'EXTRACURRICULAR',
  EXTRACURRICULAR_RESOURCE = 'EXTRACURRICULAR_RESOURCE',
  CERTIFICATE = 'CERTIFICATE',
}

@Injectable({ providedIn: 'root' })
export class GoogleAnalyticsService {
  constructor() {}

  logUser(): void {
    if (environment.production && window['firebase']) {
      window['firebase'].analytics();
    }
  }

  logEvent(eventName: string, params?: { [key: string]: any }): void {
    if (environment.production && window['firebase']) {
      window['firebase'].analytics().logEvent(eventName, params);
    }
  }
}
