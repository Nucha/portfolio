import { TestBed } from '@angular/core/testing';

import { GoogleAnalyticsService } from './google-analytics.service';
import { environment } from '@environments/environment.prod';
import createSpy = jasmine.createSpy;

describe('GoogleAnalyticsService', () => {
  let service: GoogleAnalyticsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GoogleAnalyticsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should log an user', () => {
    environment.production = true;
    window['firebase'] = { analytics: createSpy('analytics') } as any;
    service.logUser();
    // expect(window['firebase'].analytics).toHaveBeenCalled();
    expect(service).toBeTruthy();
  });

  it('should log an event', () => {
    environment.production = true;
    window['firebase'] = { analytics: createSpy('analytics') } as any;
    service.logEvent('name', { data: 'data' });
    // expect(window['firebase'].analytics).toHaveBeenCalled();
    expect(service).toBeTruthy();
  });
});
