import { Injectable } from '@angular/core';

import { Time } from '@models/time';

@Injectable({ providedIn: 'root' })
export class DateUtilsService {
  constructor() {}

  getMonthsDifference(initialDate: Date, finalDate: Date): number {
    let months;
    months = (finalDate.getFullYear() - initialDate.getFullYear()) * 12;
    months -= initialDate.getMonth();
    months += finalDate.getMonth();
    return months <= 0 ? 0 : months;
  }

  getTimeDifferenceInYearsAndMonths(initialDate: Date, finalDate: Date): Time {
    const totalMonths = this.getMonthsDifference(initialDate, finalDate);
    const monthsInYear = 12;
    const years = Math.floor(totalMonths / monthsInYear);
    const months = totalMonths - years * monthsInYear;
    return { years, months };
  }

  testFunction(d1: Date, d2: Date): void {
    const diff: Time = this.getTimeDifferenceInYearsAndMonths(d1, d2);
    console.log(`${diff.years} year and ${diff.months} months`);
  }
}
