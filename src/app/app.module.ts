import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { ToolbarComponent } from '@components/toolbar/toolbar.component';
import { SharedModule } from '@shared/shared.module';
import { SocialLinksComponent } from '@components/toolbar/social-links/social-links.component';
import { ExperienceComponent } from '@components/experience/experience.component';
import { EducationComponent } from '@components/education/education.component';
import { PersonalInformationComponent } from '@components/personal-information/personal-information.component';
import { ContactDetailsComponent } from '@components/personal-information/contact-details/contact-details.component';
import { DetailItemComponent } from '@components/personal-information/contact-details/detail-item/detail-item.component';
import { NameHeaderComponent } from '@components/toolbar/name-header/name-header.component';
import { JobDetailsComponent } from '@components/experience/job-details/job-details.component';
import { CertificationsComponent } from '@components/certifications/certifications.component';
import { JobRoleDetailComponent } from '@components/experience/job-details/job-role-detail/job-role-detail.component';
import { CertificateComponent } from '@components/certifications/certificate/certificate.component';
import { InstituteDetailsComponent } from '@components/education/institute-details/institute-details.component';
import { PeriodDetailComponent } from '@components/experience/job-details/period-detail/period-detail.component';
import { ExtracurricularComponent } from '@components/extracurricular/extracurricular.component';
import { ExtracurricularDetailComponent } from '@components/extracurricular/extracurricular-detail/extracurricular-detail.component';

const declaredComponents = [
  AppComponent,
  ToolbarComponent,
  SocialLinksComponent,
  PersonalInformationComponent,
  ExperienceComponent,
  EducationComponent,
  ContactDetailsComponent,
  DetailItemComponent,
  NameHeaderComponent,
  JobDetailsComponent,
  CertificationsComponent,
  JobRoleDetailComponent,
  CertificateComponent,
  InstituteDetailsComponent,
  PeriodDetailComponent,
  ExtracurricularComponent,
  ExtracurricularDetailComponent,
];

const importedModules = [BrowserModule, BrowserAnimationsModule, SharedModule];

@NgModule({
  declarations: declaredComponents,
  imports: importedModules,
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
