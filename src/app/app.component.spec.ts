import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { GoogleAnalyticsService } from '@services/google-analytics/google-analytics.service';
import { GoogleAnalyticsServiceMock } from '@services/google-analytics/google-analytics.service.mock';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;
  let analytics: GoogleAnalyticsServiceMock;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [AppComponent],
      providers: [
        {
          provide: GoogleAnalyticsService,
          useClass: GoogleAnalyticsServiceMock,
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should log user in analytics service when the app starts', () => {
    analytics = TestBed.inject(GoogleAnalyticsService);
    const logUserSpy = spyOn(analytics, 'logUser');
    component.ngOnInit();
    expect(analytics).toBeTruthy();
    expect(logUserSpy).toHaveBeenCalled();
  });
});
