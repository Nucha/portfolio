import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';

const angularMaterial = [MatToolbarModule, MatDividerModule, MatExpansionModule, MatCardModule, MatIconModule, MatTooltipModule];

@NgModule({
  declarations: [],
  imports: [CommonModule, ...angularMaterial],
  exports: [CommonModule, ...angularMaterial],
})
export class SharedModule {}
